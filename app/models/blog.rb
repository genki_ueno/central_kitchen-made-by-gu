class Blog < ApplicationRecord
  belongs_to :unit
  belongs_to :user

  validates :unit_id, presence: true
  validates :url,     presence: true

  default_scope -> { order(created_at: :desc) }
end

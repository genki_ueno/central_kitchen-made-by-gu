class Product < ApplicationRecord
  belongs_to :user


  CSV_ATTRIBUTES = ['stock_id', 'name', 'color', 'category', 'cost', 'group_num', 'user_id', 'stocks']

  def self.generate_csv
    CSV.generate(headers: true) do |csv|
      csv << CSV_ATTRIBUTES
      all.each do |product|
        csv << CSV_ATTRIBUTES.map{|attr| product.send(attr)}
      end
    end
  end

  def self.import(file, user_id)
    CSV.foreach(file.path, headers: true) do |row|
      product = Product.find_or_initialize_by(stock_id: row['stock_id'], user_id: user_id)
      product.update_attributes(stocks: row['stocks'])
      product.save!
    end
  end

  def self.import_from_other(file, user_id)
    CSV.foreach(file.path, headers: true) do |row|
      product = Product.find_or_initialize_by(stock_id: row[1], user_id: user_id)
      product.update_attributes(stock_id: row[1], name: row[2], color: row[3], category: row[5],
                                cost: row[6], group_num: row[9], stocks: row[11], user_id: user_id)
      product.save!
    end
  end
end

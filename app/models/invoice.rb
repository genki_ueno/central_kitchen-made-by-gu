class Invoice < ApplicationRecord
  belongs_to :sale
  belongs_to :user
  belongs_to :unit

  validates :price, presence: true
  validates :payment, presence: true
  validates :customer, presence: true

  default_scope -> { order(created_at: :desc) }

  def payment_by
    case self.payment
    when 0 then
      return "現金"
    when 1 then
      return "代引き"
    when 2 then
      return "クレカ"
    when 3 then
      return "QRコード"
    else
      return "未登録"
    end
  end
end

class Timecard < ApplicationRecord
  belongs_to :member

  validates :member_id, presence: true
  validates :start_time, presence: true

  default_scope -> { order(start_time: :asc) }
end

class Member < ApplicationRecord
  belongs_to :user
  has_many :timecards

  validates :name, presence: true
  validates :user_id, presence: true

  default_scope -> { order(created_at: :asc) }

  def working_start
    self.update_attributes( working: true)
  end
end

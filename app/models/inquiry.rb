class Inquiry < ApplicationRecord
  belongs_to :user
  validates :staff_id, :route, :products, :conditions, :results, presence: true
end

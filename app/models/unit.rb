class Unit < ApplicationRecord
  has_many :invoices
  validates :name, presence: true
end

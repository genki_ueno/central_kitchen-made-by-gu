class Ticket < ApplicationRecord
  validates :product_num, presence: true
  validates :user_num, presence: true
  validates :type_num, presence: true
  validates :count, presence: true

  def change_product_stocks(ticket, user_from_num)
    if ticket.type_num == 0..2
      product = Product.find_by(stock_id: ticket.product_num, user_id: ticket.user_num)
      product.update_attributes(stocks: product.stocks - ticket.count)
    elsif ticket.type_num == 3
      product1 = Product.find_by(stock_id: ticket.product_num, user_id: user_from_num.to_i)
      product1.update_attributes(stocks: product1.stocks - ticket.count)
      product2 = Product.find_by(stock_id: ticket.product_num, user_id: ticket.user_num)
      product2.update_attributes(stocks: product2.stocks + ticket.count)
    end
  end
end

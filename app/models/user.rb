class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :trackable

  has_many :products
  has_many :inquiries
  has_many :members
  has_many :blogs
  has_many :reviews
  has_many :sales
  has_many :invoices

  def review_count_this_month
    this_month = Time.current.all_month
    Review.where( created_at: this_month, user_id: self.id ).count
  end

  def User.user_names_without_own(current_user)
    user_names_without_own = User.where.not(id: current_user.id).map(&:name).to_a
    user_names_without_own.length.times do |index|
      user_names_without_own[index] = [user_names_without_own[index], User.find_by(name: user_names_without_own[index]).id]
    end
    user_names_without_own
  end
end

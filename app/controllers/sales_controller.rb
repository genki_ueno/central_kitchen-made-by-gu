class SalesController < ApplicationController
  def index
    user = current_user
    sale = Sale.find_by(year: Time.current.year, month: Time.current.month, day: Time.current.day)
    sale ? @sale = sale : @sale = user.sales.create(year: Time.current.year, month: Time.current.month, day: Time.current.day)
    set_sales_data
    set_doughnut_data
    @invoices = Invoice.where(user_id: current_user.id)

  end

  def create
    invoice = Invoice.new(invoice_params)
    if invoice.save
      flash.notice = '売り上げ情報を１件追加しました'
      redirect_to sales_path
    else
      flash.alert = '入力情報が不正確です'
      redirect_to sales_path
    end
  end

  private

  def invoice_params
    params.require(:invoice).permit(:user_id, :unit_id, :sale_id, :price, :payment, :customer)
  end
end

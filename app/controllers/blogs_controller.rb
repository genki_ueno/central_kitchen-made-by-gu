class BlogsController < ApplicationController
  def index
    gon.data = []
    Unit.all.count.times do |index|
      next if index.zero?
      gon.data << Blog.where(unit_id: index, created_at: Time.current.beginning_of_month..Time.current.end_of_month).count
    end
    @blogs = current_user.blogs
  end

  def new
    @user = current_user
  end

  def close
    @user = current_user
  end

  def create
    blog = Blog.new(blog_params)
    if blog.save
      flash.notice = 'ブログ投稿数を１つ増やしました'
      redirect_to root_path
    else
      flash.alert = '入力が不適切です'
      redirect_to root_path
    end
  end

  def destroy
    Blog.find(params[:id]).destroy
    flash.notice = 'ブログを１件　削除しました'
    redirect_to blogs_path
  end

  private

  def blog_params
    params.require(:blog).permit(:unit_id, :url, :user_id)
  end
end

class HomeController < ApplicationController
  before_action  :authenticate_user!, only: :index

  def index
    @user     = current_user
    set_sales_data
    set_blog_data
  end
end

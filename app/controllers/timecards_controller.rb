class TimecardsController < ApplicationController
  before_action  :authenticate_user!

  def index
    @members = Member.where(user_id: current_user.id)
  end

  def show
    @time_card = Timecard.find(params[:id])
  end

  def update
    @time_card = Timecard.find(params[:id])
    if @time_card.update_attributes(timecard_params)
      flash.notice = 'タイムカードを正常に更新しました'
      redirect_to timecards_path
    else
      render 'index'
    end
  end

  def close
    @time_card = Timecard.find(params[:id])
  end

  def destroy
    Timecard.find(params[:id]).destroy
    flash.notice = 'タイムカードを１件削除しました'
    redirect_to timecards_path
  end

  def start_working
    @member = Member.find(params[:id])
    @member.timecards.create(start_time: Time.current, date_int: Time.current.day.to_i)
    @member.update_attributes( working: true)
    respond_to do |format|
      format.html { redirect_to root_path }
      format.js
    end
  end

  def finish_working
    @member   = Member.find(params[:id])
    time_card = Timecard.find_by(member_id: @member.id, end_time: nil)
    time_card.update_attributes(end_time: Time.current)
    @member.update_attributes( working: false)
    respond_to do |format|
      format.html { redirect_to root_path }
      format.js
    end
  end

  private

  def timecard_params
    params.require(:timecard).permit(:start_time, :end_time)
  end
end

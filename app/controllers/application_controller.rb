class ApplicationController < ActionController::Base

  private
  def set_sales_data
    gon.days  = []
    gon.sales = []
    gon.sales_last_year = []
    Time.current.end_of_month.day.times do |index|
      gon.days << index + 1
      sale = Sale.find_by(year: Time.current.year, month: Time.current.month, day: index + 1)
      sale_last_year = Sale.find_by(year: Time.current.year - 1, month: Time.current.month, day: index + 1)
      if sale
        gon.sales << sale.proceeds
      else
        gon.sales << 0
      end
      if sale_last_year
        gon.sales_last_year << sale_last_year.proceeds
      else
        gon.sales_last_year << 0
      end
    end
  end

  def set_blog_data
    gon.unit_names     = []
    gon.unit_each_blog = []
    Unit.count.times do |index|
      gon.unit_names << Unit.find(index + 1).name
      gon.unit_each_blog << current_user.blogs.where(unit_id: index + 1).count
    end
  end

  def set_doughnut_data
    gon.unit_names = []
    Unit.count.times do |index|
      gon.unit_names << Unit.find(index + 1).name
    end
  end
end

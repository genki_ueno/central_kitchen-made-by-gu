class TicketsController < ApplicationController

  def create
    ticket = Ticket.new(ticket_params)
    if ticket.save
      ticket.change_product_stocks(ticket, ticket.user_from_num)
      flash.notice = '登録しました'
    else
      flash.alert = '入力に誤りがあります'
    end
    redirect_to products_path
  end

  private

  def ticket_params
    params.require(:ticket).permit(:product_num, :user_num, :type_num, :count, :user_from_num)
  end
end

class ReviewsController < ApplicationController

  def create
    current_user.reviews.create
    flash.notice = '今月のレビューを１追加しました'
    redirect_to root_path
  end
end

class InquiriesController < ApplicationController
  include HomeHelper

  def index
    set_gon_data
  end

  def create
    @inquiry = Inquiry.new(inquiry_params)
    if @inquiry.save
      flash.notice = 'ありがとうございます。問い合わせを１件、記録しました'
    else
      flash.alert = '入力に不備がありました'
    end
    redirect_to root_path
  end

  private

  def inquiry_params
    params.require(:inquiry).permit(:user_id, :staff_id, :route, :conditions, :results, :memo, :products, :series)
  end

  def set_gon_data
    gon.products_data     = []
    gon.conditions_data   = []
    gon.conditions_labels = []
    gon.results_data      = []
    gon.results_labels    = []
    Unit.all.count.times do |index|
      next if index.zero?
      gon.products_data << Inquiry.where(products: index.to_s, created_at: Time.current.beginning_of_month..Time.current.end_of_month).count
    end
    conditions.length.times do |index|
      next if index.zero?
      gon.conditions_data   << Inquiry.where(conditions: index.to_s, created_at: Time.current.beginning_of_month..Time.current.end_of_month).count
      gon.conditions_labels << conditions[index][0]
    end
    results.length.times do |index|
      next if index.zero?
      gon.results_data << Inquiry.where(results: index.to_s, created_at: Time.current.beginning_of_month..Time.current.end_of_month).count
      gon.results_labels << results[index][0]
    end
  end
end

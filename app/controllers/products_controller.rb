class ProductsController < ApplicationController
  def index
    params[:q]['name_cont_all'] = params[:q]['name_cont_all'].split(/[\p{blank}\s]+/) unless params[:q].nil?
    @q = current_user.products.ransack(params[:q])
    @products = @q.result(distinct: true)
    @ticket   = Ticket.new
    @product  = @products.first

    respond_to do |format|
      format.html
      format.csv { send_data @products.generate_csv, filename: "product-#{Time.current.strftime('%Y%m%d%H%M')}.csv" }
    end
  end

  def all_stocks
    @product  = Product.find(params[:id])
    @products = []
    User.count.times do |index|
      @products << ((p = Product.find_by(stock_id: @product.stock_id, user_id: index + 1)) ?
          [ p.user.name, p.stocks ] : [ User.find(index + 1).name, 0 ])
    end
  end

  def close

  end

  def import
    current_user.products.import(params[:file], current_user.id)
    redirect_to products_path, notice: '在庫データをインポートしました'
  end

  def import_from_other
    current_user.products.import_from_other(params[:file], current_user.id)
    redirect_to products_path, notice: '在庫データをインポートしました'
  end
end

module TimecardsHelper
  def total_worked_time(member)
    time_cards = member.timecards.where(created_at: Time.current.beginning_of_month..Time.current.end_of_month)
    total_time = { sec: 0 }
    time_cards.each do |t_card|
      total_time[:sec] += (t_card.end_time - t_card.start_time) unless t_card.end_time.nil?
    end
    total_time[:min]  = ((total_time[:sec] / 60) % 60).ceil
    total_time[:hour] = (total_time[:sec] / 3600).to_i
    total_time
  end
end

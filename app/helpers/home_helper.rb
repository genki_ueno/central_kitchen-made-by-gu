module HomeHelper

  def user_names_without_own
    User.user_names_without_own(current_user)
  end

  def a_route
    [["（必須）経路を選んで下さい", nil], "来店", "電話", "メール"]
  end

  def unit_names
    [["（必須）機種を選んでください", nil], ["iPhone", 1], ["Xperia", 2], ["Galaxy", 3], ["Zenfone", 4], ["ゲーム機", 5], ["Surface", 6], ["その他", 7]]
  end

  def staffs
    staffs  = [["（必須）スタッフを選んでください", nil]]
    members = Member.where(user_id: current_user.id)
    members.each_with_index do |member, index|
      staffs << [ member.name, member.id ]
    end
    staffs
  end

  def conditions
    [["（必須）症状を選んで下さい", nil], ["画面割れ", 1], ["バッテリー不良", 2],
     ["充電不良", 3], ["起動不良", 4], ["水没", 5],
     ["マイク不良", 6], ["スピーカー不良", 7], ["カメラ不良", 8], ["カメラレンズ割れ", 9],
     ["データ誤消去", 10], ["その他", 11]]
  end

  def results
    [["（必須）結果を選んでください", nil], ["受注", 1], ["来店予約", 2],
     ["料金や時間の確認", 3], ["失注：料金", 4], ["失注：時間", 5],
     ["失注：店舗在庫なし", 6], ["失注：取り扱いなし", 7],
     ["失注：修理できない", 8], ["失注：他にあてがある", 9]]
  end

  def current_time
    t = Time.current
    "#{t.month}月#{t.day}日　#{t.hour}時#{t.min}分"
  end

  def current_time_m_d
    t = Time.current
    "#{t.month}月#{t.day}日"
  end

  def payment
    [['現金', 0], ['代引き', 1], ['クレカ', 2], ['QRコード', 3]]
  end
end

# README

現在お世話になっているアルバイト先（スマホの修理店）で、現場を知る自分がアルバイト先に特化した社内アプリケーションを想定して開発しています。

主な機能は以下
・在庫管理
・勤怠管理
・売り上げ記録
・その他、問い合わせ情報やレビュー情報などの統計記録

主な技術情報は以下
・Ruby on Railsで作成
・Rails周辺言語も適選使用（HTML, CSS, JavaScript, Git, SQL）
・サーバーにHeroku(無料プラン)を利用
・Ajax処理
・CSV入出力（在庫データのみ）
・Gem(ransack)を利用した、複数キーワード検索機能
・Gem(devise)を利用した、ログイン周辺機能（Session, Cookies）


This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

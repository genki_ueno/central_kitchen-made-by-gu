User.create!(email: 'test@example.com', password: 'password', password_confirmation: 'password')

user_names = ['渋谷', '秋葉原', '上野', '池袋', '栄', '大須', 'なんば']
user_names.length.times do |index|
  User.create!(email: "test#{index}@example.com", password: 'password', password_confirmation: 'password', name: user_names[index])
end

user         = User.first
user.name    = '心斎橋'
user.address = '大阪府大阪市中央区西心斎橋A丁目B−C　ABCビル3F'
user.tel     = '01-2345-6789'
user.image   = 'shinsaibashi.png'
user.save

members_shinsaibasi = ['大阪　太郎', 'Staff B', 'Staff C', 'Staff D', 'Staff E']
members_jinnan      = ['松本　人志', '安倍　心臓', 'プーチン', '上田　深夜', '福沢　諭吉']

members_shinsaibasi.length.times do |n|
  User.first.members.create!( name: members_shinsaibasi[n])
end
members_jinnan.length.times do |n|
  User.second.members.create!( name: members_jinnan[n])
end

unit_names = ['iPhone', 'Xperia', 'Galaxy', 'Zenfone', 'ゲーム機', 'Surface', 'その他']

unit_names.length.times do |n|
  Unit.create!(name: unit_names[n])
end

31.times do |index|
  Sale.create(user_id: 1, proceeds: rand(50000..230000), year: 2019, month: 8, day: index + 1)
end

31.times do |index|
  Sale.create(user_id: 1, proceeds: rand(50000..230000), year: 2018, month: 8, day: index + 1)
end

30.times do |index|
  Sale.create(user_id: 1, proceeds: rand(50000..230000), year: 2019, month: 9, day: index + 1)
end

30.times do |index|
  Sale.create(user_id: 1, proceeds: rand(50000..230000), year: 2018, month: 9, day: index + 1)
end

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_11_063047) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "blogs", force: :cascade do |t|
    t.integer "user_id"
    t.integer "unit_id"
    t.string "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "inquiries", force: :cascade do |t|
    t.integer "user_id"
    t.integer "staff_id"
    t.string "route"
    t.string "conditions"
    t.string "results"
    t.text "memo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "products"
    t.string "series"
  end

  create_table "invoices", force: :cascade do |t|
    t.bigint "sale_id"
    t.bigint "user_id"
    t.bigint "unit_id"
    t.integer "price", default: 0
    t.integer "payment"
    t.string "customer", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["sale_id"], name: "index_invoices_on_sale_id"
    t.index ["unit_id"], name: "index_invoices_on_unit_id"
    t.index ["user_id"], name: "index_invoices_on_user_id"
  end

  create_table "members", force: :cascade do |t|
    t.integer "user_id"
    t.string "name"
    t.boolean "working"
    t.string "icon", default: "default_icon.png"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.string "color"
    t.string "category"
    t.float "cost", default: 0.0
    t.string "group_num"
    t.integer "stocks", default: 0
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "stock_id"
    t.index ["user_id"], name: "index_products_on_user_id"
  end

  create_table "reviews", force: :cascade do |t|
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_reviews_on_user_id"
  end

  create_table "sales", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "proceeds", default: 0
    t.integer "year"
    t.integer "month"
    t.integer "day"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_sales_on_user_id"
  end

  create_table "tickets", force: :cascade do |t|
    t.integer "product_num"
    t.integer "user_num"
    t.integer "count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "type_num"
    t.integer "user_from_num"
  end

  create_table "timecards", force: :cascade do |t|
    t.bigint "member_id"
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer "date_int"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["member_id"], name: "index_timecards_on_member_id"
  end

  create_table "units", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "address"
    t.string "tel"
    t.string "image", default: "shinsaibashi.jpg"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "invoices", "sales"
  add_foreign_key "invoices", "units"
  add_foreign_key "invoices", "users"
  add_foreign_key "products", "users"
  add_foreign_key "reviews", "users"
  add_foreign_key "sales", "users"
  add_foreign_key "timecards", "members"
end

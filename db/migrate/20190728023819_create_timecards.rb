class CreateTimecards < ActiveRecord::Migration[5.2]
  def change
    create_table :timecards do |t|
      t.references :member, foreign_key: true
      t.datetime :start_time
      t.datetime :end_time
      t.integer :date_int

      t.timestamps
    end
  end
end

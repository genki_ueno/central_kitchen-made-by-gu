class AddTypeNumToTickets < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :type_num, :integer
  end
end

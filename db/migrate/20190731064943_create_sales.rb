class CreateSales < ActiveRecord::Migration[5.2]
  def change
    create_table :sales do |t|
      t.references :user, foreign_key: true
      t.integer :proceeds, default: 0
      t.integer :year
      t.integer :month
      t.integer :day

      t.timestamps
    end
  end
end

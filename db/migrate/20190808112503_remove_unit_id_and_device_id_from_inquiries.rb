class RemoveUnitIdAndDeviceIdFromInquiries < ActiveRecord::Migration[5.2]
  def change
    remove_column :inquiries, :unit_id, :integer
    remove_column :inquiries, :device_id, :integer
  end
end

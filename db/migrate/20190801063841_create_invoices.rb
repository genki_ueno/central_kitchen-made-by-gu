class CreateInvoices < ActiveRecord::Migration[5.2]
  def change
    create_table :invoices do |t|
      t.references :sale, foreign_key: true
      t.references :user, foreign_key: true
      t.references :unit, foreign_key: true
      t.integer :price, default: 0
      t.integer :payment
      t.string :customer, default: ''

      t.timestamps
    end
  end
end

class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.string :color
      t.string :category
      t.float :cost, default: 0.0
      t.string :group_num
      t.integer :stocks, default: 0
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end

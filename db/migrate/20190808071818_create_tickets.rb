class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.integer :product_num
      t.integer :user_num
      t.integer :type
      t.integer :count

      t.timestamps
    end
  end
end

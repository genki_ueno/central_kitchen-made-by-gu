class AddProductsToInquiries < ActiveRecord::Migration[5.2]
  def change
    add_column :inquiries, :products, :string
    add_column :inquiries, :series, :string
  end
end

class CreateBlogs < ActiveRecord::Migration[5.2]
  def change
    create_table :blogs do |t|
      t.integer :user_id
      t.integer :unit_id
      t.string :url

      t.timestamps
    end
  end
end

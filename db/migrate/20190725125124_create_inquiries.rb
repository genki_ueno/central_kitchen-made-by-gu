class CreateInquiries < ActiveRecord::Migration[5.2]
  def change
    create_table :inquiries do |t|
      t.integer :user_id
      t.integer :staff_id
      t.string :route
      t.string :conditions
      t.string :results
      t.integer :unit_id
      t.integer :device_id
      t.text :memo

      t.timestamps
    end
  end
end

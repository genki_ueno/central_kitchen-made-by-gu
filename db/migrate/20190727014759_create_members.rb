class CreateMembers < ActiveRecord::Migration[5.2]
  def change
    create_table :members do |t|
      t.integer :user_id
      t.string :name
      t.boolean :working
      t.string :icon, default: 'default_icon.png'

      t.timestamps
    end
  end
end

class RemoveTypeFromTickets < ActiveRecord::Migration[5.2]
  def change
    remove_column :tickets, :type, :integer
  end
end

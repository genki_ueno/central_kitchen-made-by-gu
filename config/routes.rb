Rails.application.routes.draw do
  devise_for :users, controllers: {
      sessions: 'users/sessions'
  }

  root 'home#index'

  resources :products do
    post :import,            on: :collection
    post :import_from_other, on: :collection
    post :all_stocks,        on: :member
    get :close,              on: :collection
  end

  resources :tickets, only: :create

  resources :inquiries, only: [:index, :create]

  resources :members, only: :index

  resources :timecards do
    get :start_working,  on: :member
    get :finish_working, on: :member
    get :close,          on: :member
  end

  resources :blogs do
    get :close, on: :collection
  end

  resources :reviews, only: [:create, :destroy]

  resources :sales, only: [:index, :create]

  resources :notice, only: :new
end

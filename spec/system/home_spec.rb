require 'rails_helper'

describe 'ログイン＆トップページの表示', type: :system do
  describe 'トップページにログインした時' do

    let!(:member) { create(:member) }

    before do
      visit new_user_session_path
      fill_in 'Email', with: "test@example.com"
      fill_in 'Password', with: 'password'
      click_on 'GET STARTED'
    end

    it 'ログインが成功してトップページが表示されている' do
      expect(page).to have_content 'Signed in successfully'
      expect(page).to have_selector 'header'
      expect(page).to have_selector '.user-area'
      expect(page).to have_selector '.side-menus'
      expect(page).to have_selector '.changeable-box'
      expect(page).to have_selector '.sales-box'
      expect(page).to have_selector '.inquiry-box'
      expect(page).to have_selector '.blog-box'
      expect(page).to have_selector '.review-box'
      expect(page).to have_selector '.member-cards'
      expect(page).to have_selector 'footer'
    end

    it 'Flashメッセージのゴミ箱マークをクリックするとFlashメッセージが非表示になる' do
      find(".fa-trash-alt").click
      expect(page).not_to have_content 'You are already signed in'
    end

    it '再度ログインURLにアクセスしてもトップページにリダイレクトされる' do
      visit new_user_session_path
      expect(page).to have_content 'You are already signed in'
    end

    it '再度トップページにアクセスするとFlashメッセージが消える' do
      visit root_path
      expect(page).to have_selector 'header'
      expect(page).not_to have_content 'You are already signed in'
    end

    it 'ログアウトをクリックするとダイアログが出てきて、承認するとログインページにリダイレクトされる' do
      find('.fa-power-off').click
      expect(page.driver.browser.switch_to.alert.text).to eq "アプリケーションからログアウトします。よろしいですか？"
      page.driver.browser.switch_to.alert.accept
      expect(page).to have_selector '.login-form-box'
    end
  end
end

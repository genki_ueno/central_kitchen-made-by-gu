require 'rails_helper'

describe '問い合わせシート', type: :system do
  describe '問い合わせシートに入力' do

    let!(:member) { create(:member) }

    before do
      visit new_user_session_path
      fill_in 'Email', with: "test@example.com"
      fill_in 'Password', with: 'password'
      click_on 'GET STARTED'
      within '.staff-select' do
        select 'staff A'
      end
      within '.route-select' do
        select '来店'
      end
      within '.unit-select' do
        select 'iPhone'
      end
      fill_in 'series-field', with: '6s'
      within '.condition-select' do
        select '画面割れ'
      end
      fill_in 'memo-field', with: '地面に落として車に轢かれた'
      within '.result-select' do
        select '受注'
      end
    end

    it '全てを入力して記録をクリックするとデータの保存に成功する' do
      expect{ click_on '記録' }.to change{ Inquiry.count }.by(1)
      expect(page).to have_content 'ありがとうございます。問い合わせを１件、記録しました'
    end

    it '「スタッフ」以外入力して記録をクリックするとデータの保存に失敗する' do
      within '.staff-select' do
        select '（必須）スタッフを選んでください'
      end
      expect{ click_on '記録' }.to change{ Inquiry.count }.by(0)
      expect(page).to have_content '入力に不備がありました'
    end

    it '「経路」以外入力して記録をクリックするとデータの保存に失敗する' do
      within '.route-select' do
        select '（必須）経路を選んで下さい'
      end
      expect{ click_on '記録' }.to change{ Inquiry.count }.by(0)
      expect(page).to have_content '入力に不備がありました'
    end

    it '「機種」以外入力して記録をクリックするとデータの保存に失敗する' do
      within '.unit-select' do
        select '（必須）機種を選んでください'
      end
      expect{ click_on '記録' }.to change{ Inquiry.count }.by(0)
      expect(page).to have_content '入力に不備がありました'
    end

    it '「シリーズもしくは型番など」以外入力して記録をクリックするとデータの保存に成功する' do
      fill_in 'series-field', with: ''
      expect{ click_on '記録' }.to change{ Inquiry.count }.by(1)
      expect(page).to have_content 'ありがとうございます。問い合わせを１件、記録しました'
    end

    it '「症状」以外入力して記録をクリックするとデータの保存に失敗する' do
      within '.condition-select' do
        select '（必須）症状を選んで下さい'
      end
      expect{ click_on '記録' }.to change{ Inquiry.count }.by(0)
      expect(page).to have_content '入力に不備がありました'
    end

    it '「備考」以外入力して記録をクリックするとデータの保存に失敗する' do
      fill_in 'memo-field', with: ''
      expect{ click_on '記録' }.to change{ Inquiry.count }.by(1)
      expect(page).to have_content 'ありがとうございます。問い合わせを１件、記録しました'
    end

    it '「結果」以外入力して記録をクリックするとデータの保存に失敗する' do
      within '.result-select' do
        select '（必須）結果を選んでください'
      end
      expect{ click_on '記録' }.to change{ Inquiry.count }.by(0)
      expect(page).to have_content '入力に不備がありました'
    end

    it '何も入力せずに記録をクリックするとデータの保存に失敗する' do
      within '.staff-select' do
        select '（必須）スタッフを選んでください'
      end
      within '.route-select' do
        select '（必須）経路を選んで下さい'
      end
      within '.unit-select' do
        select '（必須）機種を選んでください'
      end
      fill_in 'series-field', with: ''
      within '.condition-select' do
        select '（必須）症状を選んで下さい'
      end
      fill_in 'memo-field', with: ''
      within '.result-select' do
        select '（必須）結果を選んでください'
      end
      expect{ click_on '記録' }.to change{ Inquiry.count }.by(0)
      expect(page).to have_content '入力に不備がありました'
    end
  end
end

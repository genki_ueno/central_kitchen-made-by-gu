FactoryBot.define do
  factory :sale do
    user { nil }
    proceeds { 1 }
    year { 1 }
    month { 1 }
    day { 1 }
  end
end

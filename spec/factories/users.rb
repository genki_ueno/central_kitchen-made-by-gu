FactoryBot.define do
  factory :user do
    sequence(:name) { |n| "test_name#{n}" }
    email { 'test@example.com' }
    password { 'password' }
    password_confirmation { 'password' }
  end
end

FactoryBot.define do
  factory :inquiry do
    user_id { 1 }
    staff_id { 1 }
    route { "MyString" }
    conditions { "MyString" }
    results { "MyString" }
    unit_id { 1 }
    devise_id { 1 }
    memo { "MyText" }
  end
end

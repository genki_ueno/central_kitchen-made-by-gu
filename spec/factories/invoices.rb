FactoryBot.define do
  factory :invoice do
    sale { nil }
    user { nil }
    unit { nil }
    price { 1 }
    payment { 1 }
    customer { "MyString" }
  end
end

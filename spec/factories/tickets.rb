FactoryBot.define do
  factory :ticket do
    product_num { 1 }
    user_num { 1 }
    type { 1 }
    count { 1 }
  end
end

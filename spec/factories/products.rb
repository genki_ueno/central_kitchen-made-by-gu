FactoryBot.define do
  factory :product do
    name { "MyString" }
    color { "MyString" }
    category { "MyString" }
    cost { 1.5 }
    group_num { "MyString" }
    stocks { 1 }
    user { nil }
  end
end
